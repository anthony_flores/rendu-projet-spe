/********************************************************************************
** Form generated from reading UI file 'menu.ui'
**
** Created by: Qt User Interface Compiler version 5.14.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MENU_H
#define UI_MENU_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_menu
{
public:
    QWidget *centralwidget;
    QPushButton *levelMaker;
    QPushButton *playButton;
    QPushButton *connection;
    QLineEdit *username;
    QLineEdit *password;
    QMenuBar *menubar;

    void setupUi(QMainWindow *menu)
    {
        if (menu->objectName().isEmpty())
            menu->setObjectName(QString::fromUtf8("menu"));
        menu->resize(183, 162);
        centralwidget = new QWidget(menu);
        centralwidget->setObjectName(QString::fromUtf8("centralwidget"));
        levelMaker = new QPushButton(centralwidget);
        levelMaker->setObjectName(QString::fromUtf8("levelMaker"));
        levelMaker->setGeometry(QRect(50, 90, 80, 26));
        playButton = new QPushButton(centralwidget);
        playButton->setObjectName(QString::fromUtf8("playButton"));
        playButton->setGeometry(QRect(50, 20, 80, 26));
        connection = new QPushButton(centralwidget);
        connection->setObjectName(QString::fromUtf8("connection"));
        connection->setGeometry(QRect(50, 90, 80, 26));
        username = new QLineEdit(centralwidget);
        username->setObjectName(QString::fromUtf8("username"));
        username->setGeometry(QRect(10, 20, 161, 26));
        password = new QLineEdit(centralwidget);
        password->setObjectName(QString::fromUtf8("password"));
        password->setGeometry(QRect(10, 50, 161, 26));
        password->setInputMethodHints(Qt::ImhHiddenText|Qt::ImhNoAutoUppercase|Qt::ImhNoPredictiveText|Qt::ImhSensitiveData);
        password->setEchoMode(QLineEdit::Password);
        menu->setCentralWidget(centralwidget);
        menubar = new QMenuBar(menu);
        menubar->setObjectName(QString::fromUtf8("menubar"));
        menubar->setGeometry(QRect(0, 0, 183, 23));
        menu->setMenuBar(menubar);

        retranslateUi(menu);

        QMetaObject::connectSlotsByName(menu);
    } // setupUi

    void retranslateUi(QMainWindow *menu)
    {
        menu->setWindowTitle(QCoreApplication::translate("menu", "Menu Principal", nullptr));
        levelMaker->setText(QCoreApplication::translate("menu", "Level Maker", nullptr));
        playButton->setText(QCoreApplication::translate("menu", "Jouer", nullptr));
        connection->setText(QCoreApplication::translate("menu", "Connexion", nullptr));
        username->setPlaceholderText(QCoreApplication::translate("menu", "username", nullptr));
        password->setPlaceholderText(QCoreApplication::translate("menu", "password", nullptr));
    } // retranslateUi

};

namespace Ui {
    class menu: public Ui_menu {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MENU_H
