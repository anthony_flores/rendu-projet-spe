/********************************************************************************
** Form generated from reading UI file 'mainwindow.ui'
**
** Created by: Qt User Interface Compiler version 5.14.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MAINWINDOW_H
#define UI_MAINWINDOW_H

#include <QtCore/QVariant>
#include <QtGui/QIcon>
#include <QtWidgets/QApplication>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_MainWindow
{
public:
    QWidget *centralwidget;
    QWidget *gridLayoutWidget;
    QGridLayout *gridLayout;
    QPushButton *Exporter;
    QPushButton *Charger;
    QGroupBox *toolBox;
    QPushButton *outilMur;
    QPushButton *outilRobot;
    QPushButton *outilNone;
    QPushButton *outilArrivee;
    QPushButton *outilPiege;
    QPushButton *outilBoue;
    QLabel *statusLabel;
    QPushButton *clearButton;
    QLineEdit *creatorLineEdit;
    QPushButton *pushButton;
    QMenuBar *menubar;

    void setupUi(QMainWindow *MainWindow)
    {
        if (MainWindow->objectName().isEmpty())
            MainWindow->setObjectName(QString::fromUtf8("MainWindow"));
        MainWindow->resize(692, 563);
        MainWindow->setMinimumSize(QSize(692, 563));
        centralwidget = new QWidget(MainWindow);
        centralwidget->setObjectName(QString::fromUtf8("centralwidget"));
        gridLayoutWidget = new QWidget(centralwidget);
        gridLayoutWidget->setObjectName(QString::fromUtf8("gridLayoutWidget"));
        gridLayoutWidget->setGeometry(QRect(9, 9, 511, 491));
        gridLayout = new QGridLayout(gridLayoutWidget);
        gridLayout->setSpacing(0);
        gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
        gridLayout->setSizeConstraint(QLayout::SetNoConstraint);
        gridLayout->setContentsMargins(0, 0, 0, 0);
        Exporter = new QPushButton(centralwidget);
        Exporter->setObjectName(QString::fromUtf8("Exporter"));
        Exporter->setGeometry(QRect(560, 30, 80, 26));
        Charger = new QPushButton(centralwidget);
        Charger->setObjectName(QString::fromUtf8("Charger"));
        Charger->setGeometry(QRect(560, 60, 80, 26));
        toolBox = new QGroupBox(centralwidget);
        toolBox->setObjectName(QString::fromUtf8("toolBox"));
        toolBox->setGeometry(QRect(530, 120, 151, 211));
        outilMur = new QPushButton(toolBox);
        outilMur->setObjectName(QString::fromUtf8("outilMur"));
        outilMur->setGeometry(QRect(10, 30, 50, 50));
        outilMur->setMinimumSize(QSize(50, 50));
        QIcon icon;
        icon.addFile(QString::fromUtf8("../Images/mur.png"), QSize(), QIcon::Normal, QIcon::Off);
        outilMur->setIcon(icon);
        outilMur->setIconSize(QSize(40, 40));
        outilRobot = new QPushButton(toolBox);
        outilRobot->setObjectName(QString::fromUtf8("outilRobot"));
        outilRobot->setGeometry(QRect(90, 30, 50, 50));
        outilRobot->setMinimumSize(QSize(50, 50));
        QIcon icon1;
        icon1.addFile(QString::fromUtf8("../Images/robot.png"), QSize(), QIcon::Normal, QIcon::Off);
        outilRobot->setIcon(icon1);
        outilRobot->setIconSize(QSize(50, 50));
        outilNone = new QPushButton(toolBox);
        outilNone->setObjectName(QString::fromUtf8("outilNone"));
        outilNone->setGeometry(QRect(90, 90, 50, 50));
        outilNone->setMinimumSize(QSize(50, 50));
        outilArrivee = new QPushButton(toolBox);
        outilArrivee->setObjectName(QString::fromUtf8("outilArrivee"));
        outilArrivee->setGeometry(QRect(10, 90, 50, 50));
        outilArrivee->setMinimumSize(QSize(50, 50));
        outilArrivee->setIconSize(QSize(50, 50));
        outilPiege = new QPushButton(toolBox);
        outilPiege->setObjectName(QString::fromUtf8("outilPiege"));
        outilPiege->setGeometry(QRect(10, 150, 50, 50));
        outilPiege->setMinimumSize(QSize(50, 50));
        outilPiege->setIconSize(QSize(50, 50));
        outilBoue = new QPushButton(toolBox);
        outilBoue->setObjectName(QString::fromUtf8("outilBoue"));
        outilBoue->setGeometry(QRect(90, 150, 50, 50));
        outilBoue->setMinimumSize(QSize(50, 50));
        outilBoue->setIconSize(QSize(50, 50));
        statusLabel = new QLabel(centralwidget);
        statusLabel->setObjectName(QString::fromUtf8("statusLabel"));
        statusLabel->setGeometry(QRect(560, 510, 121, 20));
        statusLabel->setStyleSheet(QString::fromUtf8("color: rgb(0, 255, 0);"));
        clearButton = new QPushButton(centralwidget);
        clearButton->setObjectName(QString::fromUtf8("clearButton"));
        clearButton->setGeometry(QRect(560, 90, 80, 26));
        creatorLineEdit = new QLineEdit(centralwidget);
        creatorLineEdit->setObjectName(QString::fromUtf8("creatorLineEdit"));
        creatorLineEdit->setGeometry(QRect(530, 340, 151, 26));
        pushButton = new QPushButton(centralwidget);
        pushButton->setObjectName(QString::fromUtf8("pushButton"));
        pushButton->setGeometry(QRect(560, 400, 80, 26));
        MainWindow->setCentralWidget(centralwidget);
        menubar = new QMenuBar(MainWindow);
        menubar->setObjectName(QString::fromUtf8("menubar"));
        menubar->setGeometry(QRect(0, 0, 692, 23));
        MainWindow->setMenuBar(menubar);

        retranslateUi(MainWindow);

        QMetaObject::connectSlotsByName(MainWindow);
    } // setupUi

    void retranslateUi(QMainWindow *MainWindow)
    {
        MainWindow->setWindowTitle(QCoreApplication::translate("MainWindow", "Level Maker", nullptr));
        Exporter->setText(QCoreApplication::translate("MainWindow", "Exporter", nullptr));
        Charger->setText(QCoreApplication::translate("MainWindow", "Charger", nullptr));
        toolBox->setTitle(QCoreApplication::translate("MainWindow", "Boite \303\240 outils", nullptr));
        outilMur->setText(QString());
        outilRobot->setText(QString());
        outilNone->setText(QString());
        outilArrivee->setText(QString());
        outilPiege->setText(QString());
        outilBoue->setText(QString());
        statusLabel->setText(QCoreApplication::translate("MainWindow", "Charg\303\251 avec succ\303\250s", nullptr));
        clearButton->setText(QCoreApplication::translate("MainWindow", "Effacer", nullptr));
        pushButton->setText(QCoreApplication::translate("MainWindow", "PushButton", nullptr));
    } // retranslateUi

};

namespace Ui {
    class MainWindow: public Ui_MainWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAINWINDOW_H
