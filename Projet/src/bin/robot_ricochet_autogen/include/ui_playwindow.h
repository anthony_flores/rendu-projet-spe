/********************************************************************************
** Form generated from reading UI file 'playwindow.ui'
**
** Created by: Qt User Interface Compiler version 5.14.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_PLAYWINDOW_H
#define UI_PLAYWINDOW_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QLabel>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_playwindow
{
public:
    QWidget *centralwidget;
    QWidget *gridLayoutWidget;
    QGridLayout *gridLayout;
    QPushButton *chargerButton;
    QLabel *creatorLabel;
    QLabel *dateLabel;
    QMenuBar *menubar;

    void setupUi(QMainWindow *playwindow)
    {
        if (playwindow->objectName().isEmpty())
            playwindow->setObjectName(QString::fromUtf8("playwindow"));
        playwindow->resize(530, 601);
        playwindow->setFocusPolicy(Qt::StrongFocus);
        centralwidget = new QWidget(playwindow);
        centralwidget->setObjectName(QString::fromUtf8("centralwidget"));
        gridLayoutWidget = new QWidget(centralwidget);
        gridLayoutWidget->setObjectName(QString::fromUtf8("gridLayoutWidget"));
        gridLayoutWidget->setGeometry(QRect(9, 40, 511, 491));
        gridLayout = new QGridLayout(gridLayoutWidget);
        gridLayout->setSpacing(0);
        gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
        gridLayout->setContentsMargins(0, 0, 0, 0);
        chargerButton = new QPushButton(centralwidget);
        chargerButton->setObjectName(QString::fromUtf8("chargerButton"));
        chargerButton->setGeometry(QRect(220, 540, 80, 26));
        creatorLabel = new QLabel(centralwidget);
        creatorLabel->setObjectName(QString::fromUtf8("creatorLabel"));
        creatorLabel->setGeometry(QRect(20, 10, 161, 18));
        dateLabel = new QLabel(centralwidget);
        dateLabel->setObjectName(QString::fromUtf8("dateLabel"));
        dateLabel->setGeometry(QRect(350, 10, 121, 18));
        playwindow->setCentralWidget(centralwidget);
        menubar = new QMenuBar(playwindow);
        menubar->setObjectName(QString::fromUtf8("menubar"));
        menubar->setGeometry(QRect(0, 0, 530, 23));
        playwindow->setMenuBar(menubar);

        retranslateUi(playwindow);

        QMetaObject::connectSlotsByName(playwindow);
    } // setupUi

    void retranslateUi(QMainWindow *playwindow)
    {
        playwindow->setWindowTitle(QCoreApplication::translate("playwindow", "Play", nullptr));
        chargerButton->setText(QCoreApplication::translate("playwindow", "Charger", nullptr));
        creatorLabel->setText(QCoreApplication::translate("playwindow", " Creator : ", nullptr));
        dateLabel->setText(QCoreApplication::translate("playwindow", "Date : ", nullptr));
    } // retranslateUi

};

namespace Ui {
    class playwindow: public Ui_playwindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_PLAYWINDOW_H
