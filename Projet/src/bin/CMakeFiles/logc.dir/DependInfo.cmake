# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "C"
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_C
  "/home/tinouduart/Documents/robot_ricochet/src/logSystem/src/log.c" "/home/tinouduart/Documents/robot_ricochet/src/bin/CMakeFiles/logc.dir/logSystem/src/log.c.o"
  )
set(CMAKE_C_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_C
  "QT_CORE_LIB"
  "QT_GUI_LIB"
  "QT_WIDGETS_LIB"
  "logc_EXPORTS"
  )

# The include file search paths:
set(CMAKE_C_TARGET_INCLUDE_PATH
  "logc_autogen/include"
  "../include"
  "../logSystem/include"
  )
set(CMAKE_DEPENDS_CHECK_CXX
  "/home/tinouduart/Documents/robot_ricochet/src/bin/logc_autogen/mocs_compilation.cpp" "/home/tinouduart/Documents/robot_ricochet/src/bin/CMakeFiles/logc.dir/logc_autogen/mocs_compilation.cpp.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_CXX
  "QT_CORE_LIB"
  "QT_GUI_LIB"
  "QT_WIDGETS_LIB"
  "logc_EXPORTS"
  )

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "logc_autogen/include"
  "../include"
  "../logSystem/include"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
