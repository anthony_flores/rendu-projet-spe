#ifndef DIALOG_SOLVEUR_H
#define DIALOG_SOLVEUR_H

#include <QDialog>

namespace Ui {
class Dialog_solveur;
}

class Dialog_solveur : public QDialog
{
    Q_OBJECT

public:
    explicit Dialog_solveur(QWidget *parent = nullptr);
    ~Dialog_solveur();

    void getSolvCoords(QString & str);

private:
    Ui::Dialog_solveur *ui;
};

#endif // DIALOG_SOLVEUR_H
