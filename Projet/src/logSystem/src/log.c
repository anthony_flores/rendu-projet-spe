#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <stdint.h>
#include <time.h>
#include <string.h>

#include "log.h"

char* logFileName = NULL;
int isWriting = FALSE;

int logger(int logSys, const char* functionName, int logLevel, const char* message, ...)
{
    va_list ap;
    char* pszBuffer = (char*)malloc(256);
    memset(pszBuffer, '\0', 256);

    va_start(ap, message);
    vsprintf(pszBuffer, message, ap);
    va_end(ap);

    formatAndLog(logSys, pszBuffer, functionName, logLevel);
    free(pszBuffer);
    return EXIT_SUCCESS;
}

int formatAndLog(int logSys, const char* logMsg, const char* functionName, int logLevel)
{
    char* formattedOutput = (char*)malloc(256);
    memset(formattedOutput, '\0', 256);
    char* date = (char*)malloc(20);
    memset(date, '\0', 20);
    FILE* logFile = NULL;

    char* logLevelMsg = (char*)malloc(8);
    memset(logLevelMsg, '\0', 8);

    time_t currentTime = time(NULL);
    struct tm* lt = localtime(&currentTime);
    strftime(date, 20, "%Y-%m-%d-%H:%M:%S", lt);

    if (!isWriting)
    {
        /*char* logFileName = (char*)malloc(29);*/
        logFileName = (char*)malloc(29);
        memset(logFileName, '\0', 29);
        sprintf(logFileName, "log-%s.txt", date);
    }
    isWriting = TRUE;

    switch (logLevel)
    {
    case LOG_LEVEL_INFO:
        memcpy(logLevelMsg, "info", 5);
        break;
    case LOG_LEVEL_WARNING:
        memcpy(logLevelMsg, "warning", 8);
        break;
    case LOG_LEVEL_ERROR:
        memcpy(logLevelMsg, "error", 6);
    default:
        break;
    }
    sprintf(formattedOutput, "[%s\t%s-%s] %s", date, functionName, logLevelMsg, logMsg);

    if (logSys == LOG_SYS_STDOUT)
        printf("%s\n", formattedOutput);
    else
    {
        logFile = fopen(logFileName, "a");
        fprintf(logFile, "%s", formattedOutput);
        fclose(logFile);
    }

    free(formattedOutput);
    free(logLevelMsg);
    free(date);

    return EXIT_SUCCESS;
}

void freeLog()
{
    if (logFileName)
        free(logFileName);
}

#ifdef PROGRAM
int main(int argc, char* argv[])
{
    printf("%s\n", __FUNCTION__);
    logger(LOG_SYS_STDOUT, __FUNCTION__, LOG_LEVEL_WARNING, "bonjour %s, %d ans. Comment vas-tu %s ?\n", "Quentin", 22, "Corentin");
    logger(LOG_SYS_FILE, __FUNCTION__, LOG_LEVEL_ERROR, "Salut %s mon pomme pote\n", "Quentin");
    logger(LOG_SYS_FILE, __FUNCTION__, LOG_LEVEL_ERROR, "Salut %s mon pomme pote\n", "Quentin");
    logger(LOG_SYS_FILE, __FUNCTION__, LOG_LEVEL_ERROR, "Salut %s mon pomme pote\n", "Quentin");
    freeLog();
    return 0;
}
#endif /* PROGRAM */