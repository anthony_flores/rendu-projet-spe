#ifndef MENU_H
#define MENU_H

#include <QMainWindow>

#include "mainwindow.h"
#include "playwindow.h"

namespace Ui {
class menu;
}

class menu : public QMainWindow
{
    Q_OBJECT

public:
    explicit menu(QWidget *parent = nullptr);
    ~menu();

private slots:
    void on_levelMaker_clicked();

    void on_playButton_clicked();
    void on_connection_clicked();

private:
    Ui::menu *ui;
    MainWindow* window;
    playwindow* play;
};

#endif // MENU_H
