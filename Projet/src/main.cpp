#include "mainwindow.h"
#include "menu.h"

#include "defs.h"
#include "log.h"

#include <QApplication>

int main(int argc, char *argv[])
{
    logger(LOG_SYS_FILE, __FUNCTION__, LOG_LEVEL_INFO, tabLogMsg[LOGMSG_START_APP]);
    QApplication a(argc, argv);
    menu m;
    m.show();
    //MainWindow w;
    //w.show();
    return a.exec();
}
