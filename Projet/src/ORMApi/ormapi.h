#ifndef ORMAPI_H
#define ORMAPI_H

#include "ORMApi_global.h"
#include <QxOrm.h>
#include <QxOrm_Impl.h>


/*!
    \class ORMApi
    \brief API permettant de s'interfacer avec la bibliothèque QxORM.
*/
class ORMAPI_EXPORT ORMApi
{
public:
    /* Constructeur */
    ORMApi(char* driverName, char* path, char* hostName, char* userName, char* password);

    /* int createDatabase();
     * Permet de créer une base de données en utilisant les différents champs de l'objet définis dans le constructeur.
     *
     * Renvoie 0 si tout s'est bien passé.
     */
    int createDatabase();

    /* QSqlError create_table(QSqlDatabase* pDatabase = NULL)
     * Permet de créer une table dans la base de données créée plus tôt.
     *
     * QSqlDatabase * pDatabase: paramètre optionnel permettant de préciser la base de données.
     */
    template <class T> ORMAPI_EXPORT
    QSqlError create_table(QSqlDatabase * pDatabase = NULL) /* Utilisation d'une template pour pouvoir cast sans connaître le type de base (implique aussi l'utilisation d'une fonction inline pour utiliser directement la fonction create_table de la bibliothèque QxORM) */
    { return qx::dao::detail::QxDao_CreateTable<T>::createTable(pDatabase); }

    /* QSqlError insert(T & t, QSqlDatabase * pDatabse = NULL)
     * Permet d'insérer un objet t de type T dans une base de données.
     */
    template <class T> ORMAPI_EXPORT
    QSqlError insert(T & t, QSqlDatabase * pDatabase = NULL)
    { return qx::dao::detail::QxDao_Insert<T>::insert(t, pDatabase); }

    /* QSqlError fetch_by_id(T & t, QSqlDatabase * pDatabase = NULL, const QStringList & columns = QStringList())
     * Permet de récupérer un objet via son id.
     *
     * Il faut set t->id pour ça et appeler la fonction comme suit : fetch_by_id(t);
     */
    template <class T>
    QSqlError fetch_by_id(T & t, QSqlDatabase * pDatabase = NULL, const QStringList & columns = QStringList())
    { return qx::dao::detail::QxDao_FetchById<T>::fetchById(t, pDatabase, columns); }

private:
    QString driverName; /* Nom du driver de la base de données */
    QString path;       /* Chemin vers où sauvegarder la BDD */
    QString hostName;   /* Hôte de la BDD */
    QString userName;   /* Utilisateur se connectant à la BDD */
    QString password;   /* Mot de passe de l'utilisateur */
};

#endif // ORMAPI_H
