#include "ormapi.h"

ORMApi::ORMApi(char* driverName, char* path, char* hostName, char* userName, char* password)
{
    this->driverName = QString(driverName);
    this->path = QString(path);
    this->hostName = QString(hostName);
    this->userName = QString(userName);
    this->password = QString(password);
}

int ORMApi::createDatabase()
{
    /* Init des paramètres pour communiquer avec la base de données. */
    qx::QxSqlDatabase::getSingleton()->setDriverName(this->driverName);
    qx::QxSqlDatabase::getSingleton()->setDatabaseName(this->path);
    qx::QxSqlDatabase::getSingleton()->setHostName(this->hostName);
    qx::QxSqlDatabase::getSingleton()->setUserName(this->userName);
    qx::QxSqlDatabase::getSingleton()->setPassword(this->password);

    return 0;
}
