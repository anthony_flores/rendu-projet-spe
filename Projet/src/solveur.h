#ifndef SOLVEUR_H
#define SOLVEUR_H

#include "niveau.h"

typedef struct _2dPoint
{
    int8_t x;
    int8_t y;
}_2dPoint;

typedef struct linkedList
{
    _2dPoint p;
    struct linkedList* next;
}linkedList;

class solveur
{
public:
    int8_t** adjMatrix;
    int8_t numVertices;

    /* Constructeur */
    solveur(int8_t nbVertices);
    void addEdge(int8_t x, int8_t y);
    void removeEdge(int8_t i, int8_t j);
    bool isEdge(int8_t i, int8_t j);
    void toString();
    int16_t dijkstra(int8_t** graph, int8_t src, int16_t arrivee);
    int8_t countTop(int8_t** graph);

private:
    bool isValid(int row, int col);
};

#endif // SOLVEUR_H
