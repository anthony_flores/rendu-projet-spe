#include "player.h"

#include <string.h>
#ifdef __unix__
#include <QxOrm.h>
#include <QxOrm_Impl.h>    // Automatic memory leak detection and boost serialization export macro

QX_REGISTER_CPP_MY_TEST_EXE(player)   // This macro is necessary to register 'drug' class in QxOrm context

namespace qx {
template <> void register_class(QxClass<player> & t)
{
    /* Permet de définir le nom des colonnes dans la base de données */
    t.id(& player::id, "id");
    t.data(& player::name, "name");
    t.data(& player::password, "password");
    t.data(& player::lvlId, "lvlId");
    t.data(& player::bestScore, "bestScore");
}}
#endif /* __unix__ */

player::player(std::string name, std::string password)
{
    this->name = std::string(name);
    this->password = std::string(password);
    this->lvlId = 0;
    this->bestScore = 0;
}

player::player(player* p)
{
    this->name = p->name;
    this->id = p->id;
    this->password = p->password;
    this->lvlId = p->lvlId;
    this->bestScore = p->bestScore;
}
